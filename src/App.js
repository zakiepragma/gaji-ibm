import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { Form, Row, Col, Container, InputGroup } from "react-bootstrap";
import { useState } from "react";

function App() {

  const[nama, setNama] = useState("")
  const[pendidikan, setPendidikan] = useState("slta")
  const[gajipokok, setGajipokok] = useState(800000)
  const[tenagapendidikan, setTenagapendidikan] = useState("00")
  const[tunjangantenagapendidikan, setTunjangantenagapendidikan] = useState(0)
  const[masakerja, setMasakerja] = useState(0)
  const gajiberkala = 50000
  const hasilgajiberkala = masakerja*gajiberkala
  const[jammengajar,setJammengajar] = useState(0)
  const tunjanganjammengajar = 12500
  const hasiltunjanganjammengajar = jammengajar*tunjanganjammengajar
  const[aktifkerja,setAktifkerja] = useState(26)
  const[jarakdomisili,setjarakdomisili] = useState("0")
  const[tunjangantransfortasi,setTunjangantransfortasi] = useState(2500)
  const hasiltunjangantransfortasi = aktifkerja*tunjangantransfortasi
  const tunjanganmakansiang = 8000
  const hasiltunjanganmakansiang = aktifkerja*tunjanganmakansiang
  const tunjangankesehatan = 50000
  const[tunjanganwalikelas,setTunjanganwalikelas] = useState(0)
  const[piket,setPiket] = useState(0)
  const tunjanganpiket = 18500
  const hasiltunjanganpiket = piket*tunjanganpiket
  const[bimbingansiang,setbimbingansiang] = useState(0)
  const tunjanganbimbingansiang = 12500
  const hasiltunjanganbimbingansiang = bimbingansiang*tunjanganbimbingansiang
  const[bimbinganmalam,setbimbinganmalam] = useState(0)
  const tunjanganbimbinganmalam = 32000
  const hasiltunjanganbimbinganmalam = bimbinganmalam*tunjanganbimbinganmalam
  const totalgaji =
    gajipokok+tunjangantenagapendidikan+hasilgajiberkala+hasiltunjanganjammengajar+hasiltunjangantransfortasi+hasiltunjanganmakansiang
    +tunjangankesehatan+tunjanganwalikelas+hasiltunjanganpiket+hasiltunjanganbimbingansiang+hasiltunjanganbimbinganmalam
  const[ketidakhadiran,setketidakhadiran] = useState(0)
  const pemotongantunjangantransportasi = ketidakhadiran*tunjangantransfortasi
  const pemotongantunjanganmakansiang = ketidakhadiran*tunjanganmakansiang
  const[pinjaman,setpinjaman] = useState(0)
  const totalgajibersih = (totalgaji-(pemotongantunjangantransportasi+pemotongantunjanganmakansiang))-pinjaman

  const[checked,setchecked] = useState(false)

  const handlePendidikan = (event) => {
    setPendidikan(event.target.value);

    if(event.target.value == "sd"){
      setGajipokok(600000)
    }else if(event.target.value == "sltp"){
      setGajipokok(700000)
    }else if(event.target.value == "slta"){
      setGajipokok(800000)
    }else if(event.target.value == "d1"){
      setGajipokok(900000)
    }else if(event.target.value == "d2"){
      setGajipokok(900000)
    }else if(event.target.value == "d3"){
      setGajipokok(900000)
    }else if(event.target.value == "s1"){
      setGajipokok(1250000)
    }
  };

  const handleTenagapendidikan = (event) => {
    setTenagapendidikan(event.target.value);

    if(event.target.value == "00"){
      setTunjangantenagapendidikan(0)
    }else if(event.target.value == "01"){
      setTunjangantenagapendidikan(350000)
    }else if(event.target.value == "02"){
      setTunjangantenagapendidikan(350000)
    }else if(event.target.value == "03"){
      setTunjangantenagapendidikan(350000)
    }else if(event.target.value == "04"){
      setTunjangantenagapendidikan(350000)
    }else if(event.target.value == "05"){
      setTunjangantenagapendidikan(350000)
    }else if(event.target.value == "06"){
      setTunjangantenagapendidikan(350000)
    }else if(event.target.value == "07"){
      setTunjangantenagapendidikan(350000)
    }else if(event.target.value == "08"){
      setTunjangantenagapendidikan(350000)
    }else if(event.target.value == "09"){
      setTunjangantenagapendidikan(350000)
    }else if(event.target.value == "10"){
      setTunjangantenagapendidikan(250000)
    }else if(event.target.value == "11"){
      setTunjangantenagapendidikan(250000)
    }else if(event.target.value == "12"){
      setTunjangantenagapendidikan(150000)
    }else if(event.target.value == "13"){
      setTunjangantenagapendidikan(350000)
    }else if(event.target.value == "14"){
      setTunjangantenagapendidikan(350000)
    }
  };

  const handleJarakDomisili = (event) => {
    setjarakdomisili(event.target.value);

    if(event.target.value == "0"){
      setTunjangantransfortasi(2500)
    }else if(event.target.value == "1"){
      setTunjangantransfortasi(3500)
    }else if(event.target.value == "2"){
      setTunjangantransfortasi(5000)
    }else if(event.target.value == "3"){
      setTunjangantransfortasi(7000)
    }
  };

  const handleTunjanganwalikelas = () => {
    setchecked(!checked)

    if(!checked){
      setTunjanganwalikelas(50000)
    }else{
      setTunjanganwalikelas(0)
    }
  }

  return (
    <Container className="mt-5">
      <h1 className="text-center">HITUNG GAJI PPS IBNU MAS'UD</h1>
      <h6 className="text-center">Created at : 31/10/2022 @programmercintasunnah</h6>
      <Form className="">
        <Row className="mb-3">
          <Form.Group as={Col}>
            <Form.Label>Nama</Form.Label>
            <Form.Control size="lg" placeholder="Masukkan nama" value={nama} onChange={e => setNama(e.target.value)}/>
          </Form.Group>
        </Row>

        <Row className="mb-3">
          <Col xs={2}>
            <Form.Group className="mb-3" controlId="formGridPendidikan">
            <Form.Label>Pendidikan</Form.Label>
              <Form.Select value={pendidikan} onChange={handlePendidikan}>
                <option value="sd">SD</option>
                <option value="sltp">SLTP</option>
                <option value="slta">SLTA</option>
                <option value="d1">D1</option>
                <option value="d2">D2</option>
                <option value="d3">D3</option>
                <option value="s1">S1</option>
              </Form.Select>
            </Form.Group>
          </Col>

          <Col>
            <Form.Label htmlFor="inlineFormInputGroup1">Gaji Pokok</Form.Label>
            <InputGroup className="mb-2">
              <InputGroup.Text>Rp.</InputGroup.Text>
              <Form.Control placeholder="0" readOnly value={gajipokok}/>
            </InputGroup>
          </Col>
        </Row>

        <Row className="mb-3">
          <Col xs={4}>
            <Form.Group className="mb-3" controlId="formGridPendidikan">
            <Form.Label>Jabatan Tenaga Kependidikan</Form.Label>
              <Form.Select value={tenagapendidikan} onChange={handleTenagapendidikan}>
                <option value="00">Tidak Ada</option>
                <option value="01">Waka Bidang Kurikulum</option>
                <option value="02">Waka Bidang Kesantrian Putra</option>
                <option value="03">Waka Bidang Kesantrian Putri</option>
                <option value="04">Waka Bidang Pemondokan Putra</option>
                <option value="05">Waka Bidang Pemondokan Putri</option>
                <option value="06">Waka Bidang Sarana Prasarana</option>
                <option value="07">Waka Bidang Humas</option>
                <option value="08">Tenaga ADM</option>
                <option value="09">Bendahara (Pengelola SPP)</option>
                <option value="10">Jaga Pesantren</option>
                <option value="11">Satpam Pesantren</option>
                <option value="12">Koordinator Pustaka</option>
                <option value="13">Waka Bidang Pra Tahfizh/TK</option>
                <option value="14">Koordinator Tahfizh</option>
              </Form.Select>
            </Form.Group>
          </Col>

          <Col>
            <Form.Label>Tunjangan Tenaga Kependidikan</Form.Label>
            <InputGroup className="mb-2">
              <InputGroup.Text>Rp.</InputGroup.Text>
              <Form.Control id="inlineFormInputGroup2" placeholder="0" readOnly value={tunjangantenagapendidikan}/>
            </InputGroup>
          </Col>
        </Row>

        <Row className="mb-3">
          <Col xs={3}>
            <Form.Label>Masa Kerja</Form.Label>
            <InputGroup className="mb-3">
            <Form.Control
              placeholder="0"
              value={masakerja}
              onChange={e => setMasakerja(e.target.value)}
            />
            <InputGroup.Text>Tahun</InputGroup.Text>
            </InputGroup>
          </Col>
          <Col>
            <Form.Label>Gaji Berkala</Form.Label>
            <InputGroup className="mb-2">
              <InputGroup.Text>Rp.</InputGroup.Text>
              <Form.Control placeholder="50.000" readOnly value={gajiberkala}/>
              <InputGroup.Text>/Tahun</InputGroup.Text>
            </InputGroup>
          </Col>
          <Col>
            <Form.Label>Hasil = Masa Kerja x Rp. 50.000</Form.Label>
            <InputGroup className="mb-2">
              <InputGroup.Text>Rp.</InputGroup.Text>
              <Form.Control placeholder="0" readOnly value={hasilgajiberkala}/>
            </InputGroup>
          </Col>
        </Row>

        <Row className="mb-3">
          <Col xs={3}>
            <Form.Label>Jam Mengajar</Form.Label>
            <InputGroup className="mb-3">
            <Form.Control
              placeholder="0"
              value={jammengajar}
              onChange={e => setJammengajar(e.target.value)}
            />
            <InputGroup.Text>Jam</InputGroup.Text>
            </InputGroup>
          </Col>
          <Col>
            <Form.Label>Tunjangan Jam Mengajar</Form.Label>
            <InputGroup className="mb-2">
              <InputGroup.Text>Rp.</InputGroup.Text>
              <Form.Control placeholder="12.500" readOnly value={tunjanganjammengajar}/>
              <InputGroup.Text>/Jam Pelajaran</InputGroup.Text>
            </InputGroup>
          </Col>
          <Col>
            <Form.Label>Hasil = Jam Mengajar x Rp. 12.500</Form.Label>
            <InputGroup className="mb-2">
              <InputGroup.Text>Rp.</InputGroup.Text>
              <Form.Control placeholder="0" readOnly value={hasiltunjanganjammengajar}/>
            </InputGroup>
          </Col>
        </Row>

        <Row className="mb-3">
          <Col xs={3}>
            <Form.Label>Aktif Kerja</Form.Label>
            <InputGroup className="mb-3">
              <Form.Control
                placeholder="0"
                value={aktifkerja}
                onChange={e => setAktifkerja(e.target.value)}
              />
            <InputGroup.Text>Hari</InputGroup.Text>
            </InputGroup>
          </Col>
          <Col xs={2}>
            <Form.Group className="mb-3" controlId="formGridPendidikan">
            <Form.Label>Jarak Domisili</Form.Label>
              <Form.Select value={jarakdomisili} onChange={handleJarakDomisili}>
                <option value="0">0 s.d 5 Km</option>
                <option value="1">5,01 s.d 10 Km</option>
                <option value="2">10,01 s.d 15 Km</option>
                <option value="3">Lebih 15,01 Km</option>
              </Form.Select>
            </Form.Group>
          </Col>
          <Col>
            <Form.Label>Tunjangan Transfortasi</Form.Label>
            <InputGroup className="mb-2">
              <InputGroup.Text>Rp.</InputGroup.Text>
              <Form.Control placeholder="0" readOnly value={tunjangantransfortasi}/>
            </InputGroup>
          </Col>
          <Col>
            <Form.Label>Hasil = Aktif Kerja x Tunjangan</Form.Label>
            <InputGroup className="mb-2">
              <InputGroup.Text>Rp.</InputGroup.Text>
              <Form.Control placeholder="0" readOnly value={hasiltunjangantransfortasi}/>
            </InputGroup>
          </Col>
        </Row>

        <Row className="mb-3">
          <Col xs={3}>
            
          </Col>
          <Col>
            <Form.Label>Tunjangan Makan Siang</Form.Label>
            <InputGroup className="mb-2">
              <InputGroup.Text>Rp.</InputGroup.Text>
              <Form.Control placeholder="8.000" readOnly value={tunjanganmakansiang}/>
              <InputGroup.Text>/Hari</InputGroup.Text>
            </InputGroup>
          </Col>
          <Col>
            <Form.Label>Hasil = Aktif Kerja x Tunjangan</Form.Label>
            <InputGroup className="mb-2">
              <InputGroup.Text>Rp.</InputGroup.Text>
              <Form.Control placeholder="0" readOnly value={hasiltunjanganmakansiang}/>
            </InputGroup>
          </Col>
        </Row>

        <Row className="mb-3">
          <Col>
            <Form.Label>Tunjangan Kesehatan</Form.Label>
            <InputGroup className="mb-2">
              <InputGroup.Text>Rp.</InputGroup.Text>
              <Form.Control placeholder="50.000" readOnly value={tunjangankesehatan}/>
            </InputGroup>
          </Col>
          <Col>
            <Form.Label>Tunjangan Walikelas</Form.Label>
            <InputGroup className="mb-3">
              <InputGroup.Checkbox checked={checked} onChange={handleTunjanganwalikelas}/>
              <InputGroup.Text>Rp.</InputGroup.Text>
              <Form.Control placeholder="50.000" readOnly value={tunjanganwalikelas}/>
            </InputGroup>
          </Col>
        </Row>

        <Row className="mb-3">
          <Col xs={2}>
            <Form.Label>Piket</Form.Label>
            <InputGroup className="mb-3">
            <Form.Control
              placeholder="0"
              value={piket}
              onChange={e => setPiket(e.target.value)}
            />
            <InputGroup.Text>Hari</InputGroup.Text>
            </InputGroup>
          </Col>
          <Col>
            <Form.Label>Tunjangan Piket</Form.Label>
            <InputGroup className="mb-2">
              <InputGroup.Text>Rp.</InputGroup.Text>
              <Form.Control placeholder="18.500" readOnly value={tunjanganpiket}/>
              <InputGroup.Text>/Hari (Piket)</InputGroup.Text>
            </InputGroup>
          </Col>
          <Col>
            <Form.Label>Hasil = Piket x Rp. 18.500</Form.Label>
            <InputGroup className="mb-2">
              <InputGroup.Text>Rp.</InputGroup.Text>
              <Form.Control placeholder="0" readOnly value={hasiltunjanganpiket}/>
            </InputGroup>
          </Col>
        </Row>

        <Row className="mb-3">
          <Col xs={2}>
            <Form.Label>Bimbingan Siang</Form.Label>
            <InputGroup className="mb-3">
            <Form.Control
              placeholder="0"
              value={bimbingansiang}
              onChange={e => setbimbingansiang(e.target.value)}
            />
            <InputGroup.Text>Hari</InputGroup.Text>
            </InputGroup>
          </Col>
          <Col xs={6}>
            <Form.Label>Tunjangan Bimbingan Siang</Form.Label>
            <InputGroup className="mb-2">
              <InputGroup.Text>Rp.</InputGroup.Text>
              <Form.Control placeholder="12.500" readOnly value={tunjanganbimbingansiang}/>
              <InputGroup.Text>/Hari (Setiap Bimbingan)</InputGroup.Text>
            </InputGroup>
          </Col>
          <Col>
            <Form.Label>Hasil = Bimbingan Siang x Rp. 12.500</Form.Label>
            <InputGroup className="mb-2">
              <InputGroup.Text>Rp.</InputGroup.Text>
              <Form.Control placeholder="0" readOnly value={hasiltunjanganbimbingansiang}/>
            </InputGroup>
          </Col>
        </Row>

        <Row className="mb-3">
          <Col xs={2}>
            <Form.Label>Bimbingan Malam</Form.Label>
            <InputGroup className="mb-3">
            <Form.Control
              placeholder="0"
              value={bimbinganmalam}
              onChange={e => setbimbinganmalam(e.target.value)}
            />
            <InputGroup.Text>Hari</InputGroup.Text>
            </InputGroup>
          </Col>
          <Col xs={6}>
            <Form.Label>Tunjangan Bimbingan Malam</Form.Label>
            <InputGroup className="mb-2">
              <InputGroup.Text>Rp.</InputGroup.Text>
              <Form.Control placeholder="32.000" readOnly value={tunjanganbimbinganmalam}/>
              <InputGroup.Text>/Hari (Setiap Bimbingan)</InputGroup.Text>
            </InputGroup>
          </Col>
          <Col>
            <Form.Label>Hasil = Bimbingan Malam x Rp. 32.000</Form.Label>
            <InputGroup className="mb-2">
              <InputGroup.Text>Rp.</InputGroup.Text>
              <Form.Control placeholder="0" readOnly value={hasiltunjanganbimbinganmalam}/>
            </InputGroup>
          </Col>
        </Row>

        <Row className="mb-3">
          <Form.Group as={Col} controlId="formGridTotal">
            <Form.Label>Total Gaji</Form.Label>
            <InputGroup className="mb-2">
              <InputGroup.Text>Rp.</InputGroup.Text>
              <Form.Control size="lg"  placeholder="0" readOnly value={totalgaji}/>
            </InputGroup>
          </Form.Group>
        </Row>

        <h3 className="mt-5">Pemotongan Gaji</h3>

        <Row className="mb-3">
          <Col xs={3}>
            <Form.Label>Ketidakhadiran</Form.Label>
            <InputGroup className="mb-3">
              <Form.Control
                placeholder="0"
                value={ketidakhadiran}
                onChange={e => setketidakhadiran(e.target.value)}
              />
            <InputGroup.Text>Hari</InputGroup.Text>
            </InputGroup>
          </Col>
          <Col>
            <Form.Label>Hasil = Aktif Kerja x Tunjangan Transfortasi</Form.Label>
            <InputGroup className="mb-2">
              <InputGroup.Text>Rp.</InputGroup.Text>
              <Form.Control placeholder="0" readOnly value={pemotongantunjangantransportasi}/>
            </InputGroup>
          </Col>
        </Row>

        <Row className="mb-3">
          <Col xs={3}>
            
          </Col>
          <Col>
            <Form.Label>Hasil = Ketidakhadiran x Tunjangan Makan Siang</Form.Label>
            <InputGroup className="mb-2">
              <InputGroup.Text>Rp.</InputGroup.Text>
              <Form.Control placeholder="0" readOnly value={pemotongantunjanganmakansiang}/>
            </InputGroup>
          </Col>
        </Row>

        <Row className="mb-3">
          <Col>
            <Form.Label>Pinjaman</Form.Label>
            <InputGroup className="mb-2">
              <InputGroup.Text>Rp.</InputGroup.Text>
              <Form.Control placeholder="0" value={pinjaman} onChange={e => setpinjaman(e.target.value)}/>
            </InputGroup>
          </Col>
        </Row>

        <Row className="mb-3">
          <Form.Group as={Col} controlId="formGridTotal">
            <Form.Label>Total Gaji Bersih</Form.Label>
            <InputGroup className="mb-2">
              <InputGroup.Text>Rp.</InputGroup.Text>
              <Form.Control size="lg"  placeholder="0" readOnly value={totalgajibersih}/>
            </InputGroup>
          </Form.Group>
        </Row>

      </Form>
    </Container>
  );
}

export default App;